const readline = require('readline');
const http = require('http');
const os = require('os');
const fs = require('fs');

class Program {
    constructor() {
        this.run = true;

        this.host = 'localhost';
        this.port = 3000;

        return this;
    }

    runProgram() {
        const rl = readline.createInterface(process.stdin, process.stdout);
        rl.setPrompt(`
        Choose an option:
        1. Read package.json
        2. Display OS info
        3. Start HTTP Server
        `);
        rl.prompt();

        rl.on('line', (line) => {
            switch(line.trim()) {
                case '1':
                    this.printPackageInfo();
                    break;
                case '2':
                    this.printOSInfo();
                    break;
                case '3':
                    this.startServer();
                    break;
                default:
                    console.log('Could not understand, need to type 1, 2 or 3 `' + line.trim() + '`');
                break;
            }
            rl.prompt();
        }).on('close', function() {
            console.log('ByeBye');
            process.exit(0);
        });
    }

    printOSInfo() {
        console.log('SYSTEM MEMORY:', (os.totalmem()/1024/1024/1024).toFixed(2) + 'GB');
        console.log('FREE MEMORY:', (os.freemem/1024/1024/1024).toFixed(2) + 'GB');
        console.log('CPU CORES:', os.cpus().length);
        console.log('ARCH:', os.arch());
        console.log('PLATFORM:', os.platform());
        console.log('RELEASE:', os.release());
        console.log('USER:', os.userInfo().username);
    }

    printPackageInfo(){
        fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) =>{
            console.log(content);
        })
    }

    startServer(){
        console.log('Starting HTTP server...')
				const server = http.createServer((req, res) => {
						res.statusCode = 200;
						res.setHeader('Content-Type', 'text/plain');
						res.end('Hello World');
				});
					
				server.listen(this.port);
				console.log(`listening on port ${this.port}`);
    }
}

const program = new Program();
program.runProgram();